/*
DOM - Document Object Model

    Objects are web page elements 
*/

console.log(document.querySelector('#txt-first-name'))

console.log(document)
//document refers to the whole page
//querySelector is used to select a specific element

console.log(document.getElementById("txt-first-name"))

/* 
    Alternative: 

    document.getElementById("txt-first-name")
    document.getElementsByClassName()
    document.getElementsByTagName() 
*/

const txtFirstName = document.querySelector("#txt-first-name"); 
const txtLastName = document.querySelector("#txt-last-name"); 
const spanFullName = document.querySelector('#span-full-name');


//Event Listeners
/* 
    selectedElement.addEventListener('event', function);


*/

/* txtFirstName.addEventListener('keyup', (e) => {
    spanFullName.innerHTML = txtFirstName.value
}) */

txtFirstName.addEventListener('keyup', getInput)
txtLastName.addEventListener('keyup', getInput)


function getInput() {
    let firstName = txtFirstName.value
    let lastName = txtLastName.value

    spanFullName.textContent = `${firstName} ${lastName}`
}